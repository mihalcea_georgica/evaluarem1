package evaluare.app;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public final class App {
    private App() {
    }

    private static final int SCALE = 2;
    private static final int QUANTITIES = 10;
    private static final double PRICE_FANTA = 1.3;
    private static final double PRICE_PEPSI = 1.1;
    private static final double PRICE_COLA = 1.5;
    private static final double PRICE_GIUSTO = 0.9;
    private static final double PRICE_SPRITE = 1.6;

    private static final double VALUE_05 = 0.5;
    private static final double VALUE_01 = 0.1;
    private static final double VALUE_02 = 0.2;
    private static final double VALUE_1 = 1.0;
    private static final double VALUE_2 = 2.0;

    private static final int ID_FANTA = 2;
    private static final int ID_SPRITE = 6;
    private static final int ID_COLA = 4;
    private static final int ID_PEPSI = 3;
    private static final int ID_GIUSTO = 5;

    public static void main(String[] args) {

        final PrintWriter writer = new PrintWriter(System.out);
        List<Coin> machineCoins = new ArrayList<Coin>();

        machineCoins.add(new Coin("0.1", new BigDecimal(VALUE_01).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));
        machineCoins.add(new Coin("0.2", new BigDecimal(VALUE_02).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));
        machineCoins.add(new Coin("0.5", new BigDecimal(VALUE_05).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));
        machineCoins.add(new Coin("1.0", new BigDecimal(VALUE_1).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));
        machineCoins.add(new Coin("2.0", new BigDecimal(VALUE_2).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));

        List<Drink> machineDrinks = new ArrayList<Drink>();

        machineDrinks.add(new Drink(QUANTITIES, "Fanta", ID_FANTA,
                new BigDecimal(PRICE_FANTA).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));
        machineDrinks.add(new Drink(QUANTITIES, "Pepsi", ID_PEPSI,
                new BigDecimal(PRICE_PEPSI).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));
        machineDrinks.add(new Drink(QUANTITIES, "Cola", ID_COLA,
                new BigDecimal(PRICE_COLA).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));
        machineDrinks.add(new Drink(QUANTITIES, "Giusto", ID_GIUSTO,
                new BigDecimal(PRICE_GIUSTO).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));
        machineDrinks.add(new Drink(QUANTITIES, "Sprite", ID_SPRITE,
                new BigDecimal(PRICE_SPRITE).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));

        MachineStock machineStock = new MachineStock(machineCoins,
                machineDrinks);

        Buy firstBuy = new Buy(machineStock);

        List<Coin> payment = new ArrayList<Coin>();

        // plata clientului sub diferite monezi

        payment.add(new Coin("2.0", new BigDecimal("2.0").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));
        payment.add(new Coin("0.2", new BigDecimal("0.2").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));

        // comanda clientului, cu bautura selectata si plata pentru aceasta
        DrinkAndChange takeIt = null;
        try {
            takeIt = firstBuy.buy(new Drink("Giusto", 5), payment);
        } catch (MachineException exception) {
            writer.println(exception.getMessage());
        }

        // servirea comenzii cat si a restului datorat
        if (!(takeIt == null)) {
            writer.println("Take your '"
                    + takeIt.getServedDrink().getDrinkName()
                    + "' and the change");
            final List<Coin> change = takeIt.getClientPayment();
            if (change.size() == 0) {
                writer.println("No change !");
            } else {
                for (Coin coin : change) {

                    writer.println(coin.getCoinName() + " ");
                }
            }
        }
        writer.close();

    }
}
