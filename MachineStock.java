package evaluare.app;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MachineStock {

    private List<Coin> machineCoins = new ArrayList<Coin>();
    private List<Drink> machineDrinks = new ArrayList<Drink>();

    public MachineStock(List<Coin> machineCoins, List<Drink> machineDrinks) {
        this.machineCoins = machineCoins;
        this.machineDrinks = machineDrinks;
    }

    public boolean drinkExists(Drink drink) {
        for (Drink curentDrink : machineDrinks) {
            if (curentDrink.equals(drink)) {
                return true;
            }
        }
        return false;
    }

    public List<Coin> getMachineCoins() {
        return machineCoins;
    }

    public void setMachineCoins(List<Coin> machineCoins) {
        this.machineCoins = machineCoins;
    }

    public List<Drink> getMachineDrinks() {
        return machineDrinks;
    }

    public void setMachineDrinks(List<Drink> machineDrinks) {
        this.machineDrinks = machineDrinks;
    }

    public void updateCoinsStock() {
        // daca o moneda are atributul 'cantitate '=0 atunci este scoasa ca
        // fiind indisponibila pt rest
        for (Coin curentCoin : machineCoins) {
            if (curentCoin.getCoinQuantities() == 0) {
                machineCoins.remove(curentCoin);
            }
        }
    }

    public void updateDrinksStoc() {
        // daca o bautura are atributul 'cantitate '=0 atunci este scoasa ca
        // fiind disponibila pt vanzare
        for (Drink curentDrink : machineDrinks) {
            if (curentDrink.getDrinkQuntities() == 0) {
                machineDrinks.remove(curentDrink);
            }
        }
    }

    public void takeDrink(Drink drink) {

        // vindera unei bauturi are ca efect scaderii nr de sticle de acea
        // bautura cu 1
        for (Drink curentDrink : machineDrinks) {
            if (curentDrink.equals(drink)) {
                if (curentDrink.getDrinkQuntities() > 0) {
                    Drink updateDrink = curentDrink;
                    updateDrink.setDrinkQuntities(curentDrink
                            .getDrinkQuntities() - 1);
                    machineDrinks.set(machineDrinks.indexOf(curentDrink),
                            updateDrink);
                } else {
                    PrintWriter writer = new PrintWriter(System.out);
                    writer.println("No more " + curentDrink.getDrinkName()
                            + " in Machine");
                    writer.close();
                }
            }
        }
        updateDrinksStoc();
    }

    public void useCoin(Coin coin) {
        // folosirea unei monezi in returnarea restului
        // are ca efect scaderea cu o unitate a nr de monezi de acea valoare
        Iterator<Coin> iterator = machineCoins.iterator();
        while (iterator.hasNext()) {
            Coin c = iterator.next();
            if (c.equals(coin) && c.getCoinQuantities() > 0) {
                Coin updateCoin = c;
                updateCoin.setCoinQuantities(c.getCoinQuantities() - 1);
                machineCoins.set(machineCoins.indexOf(c), updateCoin);
            }
        }
        updateCoinsStock();
    }

    /**
     * 
     * @param Cand
     *            clientul plateste,in aparat se actualizeaza stocul de coins cu
     *            adaosul platii ex : plateste cu o moneda de 2 euro, atunci
     *            cresc numarul de monezi de valoare 2 din masina cu 1
     */
    public synchronized void putCoinFromClientPayment(Coin coin) {
        for (Coin curentCoin : machineCoins) {
            if (curentCoin.equals(coin) && curentCoin.getCoinQuantities() < 10) {
                Coin updateCoin = curentCoin;
                updateCoin
                        .setCoinQuantities(curentCoin.getCoinQuantities() + 1);
                machineCoins.set(machineCoins.indexOf(curentCoin), updateCoin);
            }
        }
    }

    public BigDecimal getDrinkPrice(Drink choice) {
        for (Drink curentDrink : machineDrinks) {
            if (choice.equals(curentDrink)) {
                return curentDrink.getDrinkPrice();
            }
        }
        return BigDecimal.ZERO;
    }

}
