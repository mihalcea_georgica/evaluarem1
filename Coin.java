package evaluare.app;

import java.math.BigDecimal;

public class Coin implements Comparable<Coin> {

    private String coinName;
    private BigDecimal coinValue;
    private int coinQuantities;

    public Coin() {
        this.coinName = "";
        this.coinQuantities = 0;
        this.coinValue = BigDecimal.ZERO;
    }

    public Coin(String coinName, BigDecimal coinValue, int coinQuantities) {
        this.coinName = coinName;
        this.coinValue = coinValue;
        this.coinQuantities = coinQuantities;
    }

    public String getCoinName() {
        return coinName;
    }

    public void setCoinName(String coinName) {
        this.coinName = coinName;
    }

    public BigDecimal getCoinValue() {
        return coinValue;
    }

    public void setCoinValue(BigDecimal coinValue) {
        this.coinValue = coinValue;
    }

    public int getCoinQuantities() {
        return coinQuantities;
    }

    public void setCoinQuantities(int coinQuantities) {
        this.coinQuantities = coinQuantities;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((coinName == null) ? 0 : coinName.hashCode());
        result = prime * result + coinQuantities;
        result = prime * result
                + ((coinValue == null) ? 0 : coinValue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Coin other = (Coin) obj;
        if (coinName == null) {
            if (other.coinName != null) {
                return false;
            }
        } else if (!coinName.equals(other.coinName)) {
            return false;
        }
        if (coinQuantities != other.coinQuantities) {
            return false;
        }
        if (coinValue == null && other.coinValue != null) {
            return false;
        } else if (!coinValue.equals(other.coinValue)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Coin coin) {
        if (this.coinValue.compareTo(coin.coinValue) == -1) {
            return 1;
        }
        if (this.coinValue.compareTo(coin.coinValue) == 0) {
            return 0;
        }
        return -1;
    }

    public Coin(String coinName, BigDecimal coinValue) {
        this.coinName = coinName;
        this.coinValue = coinValue;
        this.coinQuantities = 1;
    }
}
