package evaluare.app;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Buy implements VendingMachine {

    private MachineStock machineStock;

    public MachineStock getMachineStock() {
        return machineStock;
    }

    public void setMachineStock(MachineStock machineStock) {
        this.machineStock = machineStock;
    }

    public Buy(MachineStock machineStock) {
        this.machineStock = machineStock;
    }

    public Buy() {
    }

    @Override
    public DrinkAndChange buy(Drink choice, List<Coin> clientPayment)
            throws MachineException {
        BigDecimal totalClientPayment = BigDecimal.ZERO;
        boolean paymentEnough = true;
        List<Coin> change = new ArrayList<Coin>();
        for (Coin curentCoin : clientPayment) {
            totalClientPayment = totalClientPayment.add(curentCoin
                    .getCoinValue());
            // insumez totalul platit de client pentru produsul selectat

        }

        if (machineStock.drinkExists(choice)) {
            // daca bautura este in stocul masinii,compar pretul platit de
            // client cu pretul
            // bauturii cerute

            paymentEnough = true;
            if (totalClientPayment
                    .compareTo(machineStock.getDrinkPrice(choice)) == -1) {
                paymentEnough = false;
                throw new MachineException("Not enough Coins !");
            }

            if (paymentEnough) {
                BigDecimal clientChange = BigDecimal.ZERO;
                clientChange = totalClientPayment.subtract(machineStock
                        .getDrinkPrice(choice));
                change = returnChange(clientChange,
                        machineStock.getMachineCoins());

            }
        } else {
            throw new MachineException("No drink available");
        }
        // plata clientului se transforma in monede ce pot fi date ulterior ca
        // rest !
        for (Coin curentCoin : clientPayment) {
            machineStock.putCoinFromClientPayment(curentCoin);
        }
        DrinkAndChange retVal = new DrinkAndChange(change, choice);

        return retVal;
    }

    public synchronized List<Coin> returnChange(BigDecimal change,
            List<Coin> listCoins) {

        List<Coin> changeCoinList = new ArrayList<Coin>();

        Collections.sort(listCoins);

        BigDecimal clientChange = BigDecimal.ZERO;

        for (Coin curentCoin : listCoins) {

            while ((clientChange.add((curentCoin.getCoinValue())).compareTo(
                    change) == -1)
                    || (clientChange.add((curentCoin.getCoinValue()))
                            .compareTo(change) == 0)) {

                clientChange = clientChange.add((curentCoin.getCoinValue()));

                changeCoinList.add(new Coin(curentCoin.getCoinName(),
                        curentCoin.getCoinValue(), 1));

                machineStock.useCoin(curentCoin);
                // foloseste moneda din stocul masinii
            }
        }
        return changeCoinList;
    }
}
