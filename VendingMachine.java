package evaluare.app;

import java.math.BigDecimal;
import java.util.List;

public interface VendingMachine {

    DrinkAndChange buy(Drink choice, List<Coin> clientPayment)
            throws MachineException;

    List<Coin> returnChange(BigDecimal change, List<Coin> listCoins);

}
