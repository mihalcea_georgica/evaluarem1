package evaluare.app;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class Tests {

    private static final int SCALE = 2;
    private static final int QUANTITIES = 10;

    private static final double PRICE_FANTA = 1.3;
    private static final double PRICE_PEPSI = 1.1;
    private static final double PRICE_COLA = 1.5;
    private static final double PRICE_GIUSTO = 0.9;
    private static final double PRICE_SPRITE = 1.6;

    private static final double VALUE_05 = 0.5;
    private static final double VALUE_01 = 0.1;
    private static final double VALUE_02 = 0.2;
    private static final double VALUE_1 = 1.0;
    private static final double VALUE_2 = 2.0;

    private static final int ID_FANTA = 2;
    private static final int ID_SPRITE = 6;
    private static final int ID_COLA = 4;
    private static final int ID_PEPSI = 3;
    private static final int ID_GIUSTO = 5;

    List<Coin> machineCoins = new ArrayList<Coin>();
    List<Drink> machineDrinks = new ArrayList<Drink>();

    MachineStock machineStock = new MachineStock(machineCoins, machineDrinks);

    Buy firstBuy = new Buy(machineStock);

    List<Coin> payment = new ArrayList<Coin>();

    public Tests() {
        machineCoins.add(new Coin("0.1", new BigDecimal(VALUE_01).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));
        machineCoins.add(new Coin("0.2", new BigDecimal(VALUE_02).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));
        machineCoins.add(new Coin("0.5", new BigDecimal(VALUE_05).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));
        machineCoins.add(new Coin("1.0", new BigDecimal(VALUE_1).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));
        machineCoins.add(new Coin("2.0", new BigDecimal(VALUE_2).setScale(
                SCALE, BigDecimal.ROUND_FLOOR), QUANTITIES));

        //

        machineDrinks.add(new Drink(QUANTITIES, "Fanta", ID_FANTA,
                new BigDecimal(PRICE_FANTA).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));
        machineDrinks.add(new Drink(QUANTITIES, "Pepsi", ID_PEPSI,
                new BigDecimal(PRICE_PEPSI).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));
        machineDrinks.add(new Drink(QUANTITIES, "Cola", ID_COLA,
                new BigDecimal(PRICE_COLA).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));
        machineDrinks.add(new Drink(QUANTITIES, "Giusto", ID_GIUSTO,
                new BigDecimal(PRICE_GIUSTO).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));
        machineDrinks.add(new Drink(QUANTITIES, "Sprite", ID_SPRITE,
                new BigDecimal(PRICE_SPRITE).setScale(SCALE,
                        BigDecimal.ROUND_FLOOR)));
    }

    @Test
    public void testBuyFanta() throws MachineException {

        // Fanta test
        payment.add(new Coin("1.5", new BigDecimal("1.0").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));
        payment.add(new Coin("0.5", new BigDecimal("0.5").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));

        DrinkAndChange buySomething = firstBuy.buy(
                new Drink("Fanta", ID_FANTA), payment);

        List<Coin> expectedResult = new ArrayList<Coin>();

        // change expected for a Fanta ,after the total payment

        expectedResult.add(new Coin("0.2", new BigDecimal("0.2").setScale(2,
                BigDecimal.ROUND_FLOOR)));

        Assert.assertEquals(buySomething.getClientPayment(), expectedResult);
        Assert.assertEquals(new Drink("Fanta", ID_FANTA),
                buySomething.getServedDrink());

    }

    @Test
    public void testBuyPepsi() throws MachineException {

        // Pepsi test
        payment.add(new Coin("2.0", new BigDecimal("2.0").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));
        payment.add(new Coin("0.5", new BigDecimal("0.5").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));

        DrinkAndChange buySomething = firstBuy.buy(
                new Drink("Pepsi", ID_PEPSI), payment);

        List<Coin> expectedResult = new ArrayList<Coin>();

        // change expected for a Pepsi ,after the total payment

        expectedResult.add(new Coin("1.0", new BigDecimal("1.0").setScale(2,
                BigDecimal.ROUND_FLOOR)));
        expectedResult.add(new Coin("0.2", new BigDecimal("0.2").setScale(2,
                BigDecimal.ROUND_FLOOR)));
        expectedResult.add(new Coin("0.2", new BigDecimal("0.2").setScale(2,
                BigDecimal.ROUND_FLOOR)));

        Assert.assertEquals("Expected != found !",
                buySomething.getClientPayment(), expectedResult);
        Assert.assertEquals("Expected !=found !", new Drink("Pepsi", ID_PEPSI),
                buySomething.getServedDrink());

    }

    @Test
    public void testBuyGiusto() throws MachineException {

        // Giusto test
        payment.add(new Coin("2.0", new BigDecimal("2.0").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));
        payment.add(new Coin("0.2", new BigDecimal("0.2").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));

        DrinkAndChange buySomething = firstBuy.buy(new Drink("Giusto",
                ID_GIUSTO), payment);

        List<Coin> expectedResult = new ArrayList<Coin>();

        // change expected for a Giusto ,after the total payment

        expectedResult.add(new Coin("1.0", new BigDecimal("1.0").setScale(2,
                BigDecimal.ROUND_FLOOR)));
        expectedResult.add(new Coin("0.2", new BigDecimal("0.2").setScale(2,
                BigDecimal.ROUND_FLOOR)));
        expectedResult.add(new Coin("0.1", new BigDecimal("0.1").setScale(2,
                BigDecimal.ROUND_FLOOR)));

        Assert.assertEquals("Expected != found !",
                buySomething.getClientPayment(), expectedResult);

        Assert.assertEquals("Expected !=found !",
                new Drink("Giusto", ID_GIUSTO), buySomething.getServedDrink());

    }

    @Test
    public void testReturnChange() {

        List<Coin> expectedCoins = new ArrayList<Coin>();

        expectedCoins.add(new Coin("0.2", new BigDecimal("0.2").setScale(2,
                BigDecimal.ROUND_FLOOR)));
        List<Coin> resultReturned = firstBuy.returnChange(
                new BigDecimal("0.2").setScale(2, BigDecimal.ROUND_FLOOR),
                machineStock.getMachineCoins());

        Assert.assertEquals(expectedCoins, resultReturned);
    }

    @Test
    public void testBuyExteptionNotEnoughPayment() {

        // payment < price => throw Exception
        payment.add(new Coin("0.5", new BigDecimal("0.5").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));
        payment.add(new Coin("0.2", new BigDecimal("0.2").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));

        boolean exception = false;
        try {
            final DrinkAndChange buySomething = firstBuy.buy(new Drink("Pepsi",
                    ID_PEPSI), payment);

        } catch (MachineException ex) {
            exception = true;
        }
        assertTrue(exception);
    }

    @Test
    public void testBuyExteptionDrinkNotAvaillable() {

        payment.add(new Coin("1.0", new BigDecimal("1.0").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));
        payment.add(new Coin("0.5", new BigDecimal("0.5").setScale(2,
                BigDecimal.ROUND_FLOOR), 1));

        boolean exception = false;
        try {
            DrinkAndChange buySomething = firstBuy.buy(new Drink("Adria", 3),
                    payment);

            // Adria nu este in stoc
        } catch (MachineException ex) {

            exception = true;
        }
        assertTrue(exception);
    }
}
