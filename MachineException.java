package evaluare.app;

public class MachineException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public MachineException(String exception) {
        super(exception);
    }

}
