package evaluare.app;

import java.math.BigDecimal;

public class Drink {

    private int drinkQuntities;
    private String drinkName;
    private int drinkId;
    private BigDecimal drinkPrice;

    public Drink(String drinkName, int drinkId) {
        this.drinkName = drinkName;
        this.drinkId = drinkId;
        drinkPrice = BigDecimal.ZERO;
        drinkQuntities = 1;
    }

    public Drink(int drinkQuntities, String drinkName, int drinkId,
            BigDecimal drinkPrice) {
        this.drinkQuntities = drinkQuntities;
        this.drinkName = drinkName;
        this.drinkId = drinkId;
        this.drinkPrice = drinkPrice;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + drinkId;
        result = prime * result
                + ((drinkName == null) ? 0 : drinkName.hashCode());
        result = prime * result
                + ((drinkPrice == null) ? 0 : drinkPrice.hashCode());
        result = prime * result + drinkQuntities;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Drink other = (Drink) obj;
        if (drinkId != other.drinkId) {
            return false;
        }
        if (drinkName == null && other.drinkName != null) {
            return false;

        } else if (!drinkName.equals(other.drinkName)) {
            return false;
        }
        return true;
    }

    public int getDrinkQuntities() {
        return drinkQuntities;
    }

    public void setDrinkQuntities(int drinkQuntities) {
        this.drinkQuntities = drinkQuntities;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public void setDrinkName(String drinkName) {
        this.drinkName = drinkName;
    }

    public int getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(int drinkId) {
        this.drinkId = drinkId;
    }

    public BigDecimal getDrinkPrice() {
        return drinkPrice;
    }

    public void setDrinkPrice(BigDecimal drinkPrice) {
        this.drinkPrice = drinkPrice;
    }

}
