package evaluare.app;

import java.util.List;

public class DrinkAndChange {

    private List<Coin> clientPayment;
    private Drink servedDrink;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((clientPayment == null) ? 0 : clientPayment.hashCode());
        result = prime * result
                + ((servedDrink == null) ? 0 : servedDrink.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        DrinkAndChange other = (DrinkAndChange) obj;
        if (clientPayment == null && other.clientPayment != null) {
            return false;
        } else if (!clientPayment.equals(other.clientPayment)) {
            return false;
        }
        if (servedDrink == null && other.servedDrink != null) {

            return false;
        } else if (!servedDrink.equals(other.servedDrink)) {
            return false;
        }
        return true;
    }

    public DrinkAndChange(List<Coin> clientPayment, Drink servedDrink) {
        this.clientPayment = clientPayment;
        this.servedDrink = servedDrink;
    }

    public List<Coin> getClientPayment() {
        return clientPayment;
    }

    public void setClientPayment(List<Coin> clientPayment) {
        this.clientPayment = clientPayment;
    }

    public Drink getServedDrink() {
        return servedDrink;
    }

    public void setServedDrink(Drink servedDrink) {
        this.servedDrink = servedDrink;
    }

}
